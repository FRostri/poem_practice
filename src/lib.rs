use poem_openapi::Tags;

pub mod responses;
pub mod schemas;
pub mod services;

#[derive(Tags)]
pub enum ApiTags {
    User,
    Post,
}
