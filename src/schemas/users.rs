use poem_openapi::Object;

/// User schema
#[derive(Debug, Object, Clone, Eq, PartialEq)]
pub struct User {
    /// User id
    #[oai(read_only)]
    pub id: u64,
    /// User name
    #[oai(validator(max_length = 64))]
    pub name: String,
    /// User password
    #[oai(validator(max_length = 32))]
    pub password: String,
    /// User email
    pub email: String,
}

#[derive(Debug, Object, Clone, Eq, PartialEq)]
pub struct UpdateUser {
    /// User name
    pub name: String,
    /// User password
    pub password: String,
}
