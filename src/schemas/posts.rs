use poem_openapi::Object;

/// User schema
#[derive(Debug, Object, Clone, Eq, PartialEq)]
pub struct Post {
    /// User id
    #[oai(read_only)]
    pub id: u64,
    /// Post title
    pub title: String,
    /// Post content
    pub content: String,
}

#[derive(Debug, Object, Clone, Eq, PartialEq)]
pub struct UpdatePost {
    /// Post title
    pub title: String,
    /// Post content
    pub content: String,
}
