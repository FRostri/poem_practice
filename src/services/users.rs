use crate::responses::users::{
    CreateUserResponse, DeleteUserResponse, FindUserResponse, UpdateUserResponse,
};
use crate::schemas::users::{UpdateUser, User};
use crate::ApiTags;
use poem::web::Path;
use poem_openapi::{payload::Json, OpenApi};
use slab::Slab;
use tokio::sync::Mutex;

#[derive(Default)]
pub struct UserService {
    users: Mutex<Slab<User>>,
}

#[OpenApi]
impl UserService {
    /// Find a user by id
    #[oai(path = "/:id", method = "get", tag = "ApiTags::User")]
    async fn find_user(&self, user_id: Path<i64>) -> FindUserResponse {
        let users = self.users.lock().await;
        match users.get(user_id.0 as usize) {
            Some(user) => FindUserResponse::Ok(Json(user.clone())),
            None => FindUserResponse::NotFound,
        }
    }

    /// Creates a new user
    #[oai(path = "/", method = "post", tag = "ApiTags::User")]
    async fn create_user(&self, user: Json<User>) -> CreateUserResponse {
        let mut users = self.users.lock().await;
        let id = users.insert(user.0) as i64;
        CreateUserResponse::Ok(Json(id))
    }

    /// Update user data
    #[oai(path = "/:id", method = "put", tag = "ApiTags::User")]
    async fn update_user(
        &self,
        user_id: Path<i64>,
        update: Json<UpdateUser>,
    ) -> UpdateUserResponse {
        let mut users = self.users.lock().await;
        match users.get_mut(user_id.0 as usize) {
            Some(user) => {
                user.name = update.0.name;
                user.password = update.0.password;
                UpdateUserResponse::Ok
            }
            None => UpdateUserResponse::NotFound,
        }
    }

    /// Delete a user
    #[oai(path = "/:id", method = "delete", tag = "ApiTags::User")]
    async fn delete_user(&self, user_id: Path<i64>) -> DeleteUserResponse {
        let mut users = self.users.lock().await;
        let user_id = user_id.0 as usize;
        if users.contains(user_id) {
            users.remove(user_id);
            DeleteUserResponse::Ok
        } else {
            DeleteUserResponse::NotFound
        }
    }
}
