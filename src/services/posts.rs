use crate::responses::posts::{
    CreatePostResponse, DeletePostResponse, FindPostResponse, UpdatePostResponse,
};
use crate::schemas::posts::{Post, UpdatePost};
use crate::ApiTags;
use poem::web::Path;
use poem_openapi::{payload::Json, OpenApi};
use slab::Slab;
use tokio::sync::Mutex;

#[derive(Default)]
pub struct PostService {
    posts: Mutex<Slab<Post>>,
}

#[OpenApi]
impl PostService {
    #[oai(path = "/:id", method = "get", tag = "ApiTags::Post")]
    async fn find_posts(&self, post_id: Path<u64>) -> FindPostResponse {
        let posts = self.posts.lock().await;
        match posts.get(post_id.0 as usize) {
            Some(post) => FindPostResponse::Ok(Json(post.clone())),
            None => FindPostResponse::NotFound,
        }
    }

    #[oai(path = "/", method = "post", tag = "ApiTags::Post")]
    async fn create_post(&self, post: Json<Post>) -> CreatePostResponse {
        let mut posts = self.posts.lock().await;
        let id = posts.insert(post.0) as i64;
        CreatePostResponse::Ok(Json(id))
    }

    #[oai(path = "/:id", method = "put", tag = "ApiTags::Post")]
    async fn update_post(
        &self,
        update: Json<UpdatePost>,
        post_id: Path<u64>,
    ) -> UpdatePostResponse {
        let mut posts = self.posts.lock().await;
        match posts.get_mut(post_id.0 as usize) {
            Some(post) => {
                post.title = update.0.title;
                post.content = update.0.content;
                UpdatePostResponse::Ok
            }
            None => UpdatePostResponse::NotFound,
        }
    }

    #[oai(path = "/:id", method = "delete", tag = "ApiTags::Post")]
    async fn delete_post(&self, post_id: Path<u64>) -> DeletePostResponse {
        let mut posts = self.posts.lock().await;
        if posts.contains(post_id.0 as usize) {
            DeletePostResponse::Ok(Json(posts.remove(post_id.0 as usize)))
        } else {
            DeletePostResponse::NotFound
        }
    }
}
