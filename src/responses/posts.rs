use crate::schemas::posts::Post;
use poem_openapi::{payload::Json, ApiResponse};

#[derive(ApiResponse)]
pub enum CreatePostResponse {
    /// Returns when the user is successfully created
    #[oai(status = 200)]
    Ok(Json<i64>),
}

#[derive(ApiResponse)]
pub enum FindPostResponse {
    /// Returns when the server find a user
    #[oai(status = 200)]
    Ok(Json<Post>),
    /// Returns if there's no user with the specified id
    #[oai(status = 404)]
    NotFound,
}

#[derive(ApiResponse)]
pub enum UpdatePostResponse {
    /// Returns when the user was successfully updated
    #[oai(status = 200)]
    Ok,
    /// Returns if there's no user with the specified id
    #[oai(status = 404)]
    NotFound,
}

#[derive(ApiResponse)]
pub enum DeletePostResponse {
    /// Returns when the user was successfully deleted
    #[oai(status = 200)]
    Ok(Json<Post>),
    /// Returns if there's no user with the specified id
    #[oai(status = 404)]
    NotFound,
}
