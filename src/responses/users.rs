use crate::schemas::users::User;
use poem_openapi::{payload::Json, ApiResponse};

#[derive(ApiResponse)]
pub enum CreateUserResponse {
    /// Returns when the user is successfully created
    #[oai(status = 200)]
    Ok(Json<i64>),
}

#[derive(ApiResponse)]
pub enum FindUserResponse {
    /// Returns when the server find a user
    #[oai(status = 200)]
    Ok(Json<User>),
    /// Returns if there's no user with the specified id
    #[oai(status = 404)]
    NotFound,
}

#[derive(ApiResponse)]
pub enum UpdateUserResponse {
    /// Returns when the user was successfully updated
    #[oai(status = 200)]
    Ok,
    /// Returns if there's no user with the specified id
    #[oai(status = 404)]
    NotFound,
}

#[derive(ApiResponse)]
pub enum DeleteUserResponse {
    /// Returns when the user was successfully deleted
    #[oai(status = 200)]
    Ok,
    /// Returns if there's no user with the specified id
    #[oai(status = 404)]
    NotFound,
}
