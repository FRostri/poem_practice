use poem::{listener::TcpListener, Route, Server};
use poem_openapi::OpenApiService;
use poem_practice::services::{posts::PostService, users::UserService};

#[tokio::main]
async fn main() -> Result<(), std::io::Error> {
    if std::env::var_os("RUST_LOG").is_none() {
        std::env::set_var("RUST_LOG", "poem=debug");
    }
    tracing_subscriber::fmt::init();

    let user_service = OpenApiService::new(UserService::default(), "Users", "1.0.0")
        .server("http://localhost:3000/api/users");
    let post_service = OpenApiService::new(PostService::default(), "Posts", "1.0.0")
        .server("http://localhost:3000/api/posts");
    let ui = user_service.swagger_ui();
    let ui_p = post_service.swagger_ui();

    Server::new(TcpListener::bind("127.0.0.1:3000"))
        .run(
            Route::new()
                .nest("/api/users", user_service)
                .nest("/api/posts", post_service)
                .nest("/swagger_us", ui)
                .nest("/swagger_po", ui_p),
        )
        .await
}
